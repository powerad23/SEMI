// Scripting Engine for Melvor Idle v0.3.5 by aldousWatts on GitLab | Built for Melvor Idle alpha v0.14.2
// Currently developing on Waterfox 2020.02 KDE Plasma Edition (56.3), latest Chromium, and Latest Ubuntu & Android Firefox.
// As always, use and modify at your own risk. But hey, contribute and share!
// This code is open source and shared freely under MPL/GNUv3/creative commons licenses.

//Injecting Scripts

var isChrome = navigator.userAgent.match("Chrome");
var isFirefox = navigator.userAgent.match("Firefox");

var getURL = (name) => (isChrome ? chrome : browser).runtime.getURL(name)

// Check if script already exists, if so delete it
function removeIfExists(scriptID) {
    const el = document.getElementById(scriptID);
    if (document.contains(el)) { el.remove(); }
};

// Inject script
function addScript(name, scriptID) {
    const script = document.createElement('script');
    script.src = getURL(name);
    script.setAttribute('id', scriptID);
    document.body.appendChild(script);
    return script;
}

function removeAndAddScript(name, scriptID) {
    removeIfExists(scriptID);
    addScript(name, scriptID);
}

function addPlugin(name) { removeAndAddScript(`scripts/plugins/${name}.js`, `SEMI-${name}`) }

// Create image element
function createImage(name, imgId, height = 32, width = 32) {
    const img = document.createElement('img');
    img.src = getURL(name);
    img.id = imgId;
    img.height = height;
    img.width = width;
    return img;
}

var autoNames = ['arch', 'easter', 'bonfire', 'combat', 'cook', 'eat', 'fish', 'mine', 'replant', 'sell-gems', 'slayer', 'smith', 'sell-fish-junk'];
var pluginNames = ['menus', ...autoNames.map((name) => `auto-${name}`), 'barf', 'calc-to-level', 'destroy-crops', 'katorone','thief-calc', 'xp-per-hour'];

function main() {
    // Only support firefox and chrome. To allow loading on other browsers, delete this entire if statement including brackets & contents {}.
    if(!isChrome && !isFirefox) {
        alert("SEMI is only officially supported on Firefox and Chrome. To try on another browser, you must modify the main() function in SEMI.js. The addon will not load otherwise.");
        return;
    }

    removeAndAddScript('scripts/core.js', 'semi-inject-core'); //adding core for refactoring
    pluginNames.forEach(addPlugin);
    removeAndAddScript('scripts/utils.js', 'semi-inject-utils');

    // not sure how to get the icon otherwise. need to leave the heading addition here, could probably just copy the rest to a big injection.
    if (document.contains(document.getElementById('modal-semi-set-menu')) ) { return; }
    const navbar = document.getElementsByClassName("nav-main")[0];
    const clnheading = document.getElementsByClassName("nav-main-heading")[1].cloneNode(true); // //in MIv0.13 pulls up the main nav version header. used to use two lines, used to be heading then clnheading
    navbar.appendChild(clnheading);
    clnheading.style = "font-size: 12pt; color: gold;";
    clnheading.childNodes[0].textContent = " SEMI v0.3.5";
    clnheading.title = "Scripting Engine for Melvor Idle";
    clnheading.id = "semiHeading";
    const iconImg = createImage("icons/border-48.png", 'iconImg');
    clnheading.insertBefore(iconImg, clnheading.childNodes[0]);
}

main();
