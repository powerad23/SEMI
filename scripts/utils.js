//custom notifications! green background with custom txt, two images, second one optional, main one is add-on icon.
function customNotify(imgsrc="", msg="Custom Notifications!", n=3000) { //outputs a custom notification with optional first image, SEMI icon, and message.
    $.notify(
		{
			message: '<img class="notification-img" src="' + imgsrc + '"><img src="'+ $("#iconImg").attr('src') +'" height="auto" width="auto" style="margin: 4px;"><span class="badge badge-success">' + msg + '</span>'
		},
		{
			type: 'light',
			placement: {
				from: 'bottom',
				align: 'center'
			},
			delay: n,
			newest_on_top: true,
			animate: {
				enter: 'animated fadeInUp',
				exit: 'animated fadeOut'
			},
			template: '<div data-notify="container" class="col-12 text-center notify-event" role="alert"><span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>'
		}
	);
}

function getBankQty(id) {
    for (let i = 0; i < bank.length; i++) {
		if (bank[i].id === id) {
			return bank[i].qty;
		}
    }
    return 0;
}

/* more efficient by rebelEpik, however seems to break AutoSellGems
function getBankQty(id) {
    var t = bank.find(x=> x.id === id);
    if(t > 0){
        return t; //tried t.qty, no luck.
    }else{
        return 0;
    }
}
*/
